﻿namespace JobDeleteFiles
{
    public static class AppSettings
    {
        private static readonly object Locker = new object();

        private static string _printedDir = string.Empty;

        public static string PrintedDir
        {
            get => _printedDir;
            set
            {
                lock (Locker)
                {
                    _printedDir = value;
                }
            }
        }

        private static int _timer = 0;

        public static int Timer
        {
            get => _timer;
            set
            {
                lock (Locker)
                {
                    _timer = value;
                }
            }
        }
    }
}