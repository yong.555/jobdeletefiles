﻿using Microsoft.Extensions.Configuration;

namespace JobDeleteFiles
{
    public class Startup
    {
        private IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"{Environment.CurrentDirectory}\\appsettings.json");

            Configuration = builder.Build();

            AppSettings.PrintedDir = Configuration.GetValue<string>("Storages:PrintedDir");
            AppSettings.Timer = int.TryParse(Configuration.GetValue<string>("Timer:DeleteFile"), out int value) ? value : 0;
        }
    }
}