﻿namespace JobDeleteFiles
{
    public class Program
    {
        private static void Main(string[] args)
        {
            InitializeConfiguration();
            Run();
        }

        public static void InitializeConfiguration()
        {
            try
            {
                _ = new Startup();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("::::::::::::Exception:::::::::::: {0}", ex.Message));
                Exit();
            }
        }

        private static void Exit() => Environment.Exit(1);
        private static void Run()
        {
            try
            {
                #region [Begin]

                Console.WriteLine(string.Format("::::::::::::Start::::::::::::  {0}", DateTime.Now.ToString()));

                #endregion [Begin]

                #region [Functions]

                var dirName = AppSettings.PrintedDir;
                if (!Directory.Exists(dirName)) Exit();

                string[] files = Directory.GetFiles(dirName);
                foreach (string file in files)
                {
                    var fi = new FileInfo(file);
                    if (fi.LastWriteTime.Date < DateTime.Now.AddDays(AppSettings.Timer).Date)
                        fi.Delete();
                }

                #endregion [Functions]

                #region [End]

                Console.WriteLine(string.Format("::::::::::::End:::::::::::: {0}", DateTime.Now.ToString()));

                #endregion [End]
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("::::::::::::Exception:::::::::::: {0}", ex.Message));
            }
        }
    }
}